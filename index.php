<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 10/02/2018
 * Time: 10:13 AM
 */

require("core/core.php");


if (isset($_GET['v'])){

    if (file_exists("core/controller/".strtolower($_GET['v']).".php")){
        include("core/controller/".strtolower($_GET['v']).".php");
    }
    else{
        include("core/controller/home.php");
    }

}
else{
    include("core/controller/home.php");
}