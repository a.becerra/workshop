<nav>
    <h2><a href="#" id="logo"><?php echo APP_TITLE ?></a></h2>
    <button class="nav-button fa fa-bars"></button>
    <div>
        <!--     <ul> -->
        <ul>
            <button class="exit-menu fa fa-times"></button>
            <li><a href="#" class="active">welcome</a></li>
            <li><a href="#">menu</a></li>
            <li><a href="#">reservations</a></li>
            <li><a href="#">news</a></li>
            <li><a href="#">contact</a></li>
        </ul>
        <!--     </ul> -->
    </div>
</nav>