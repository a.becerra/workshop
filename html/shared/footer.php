<footer>
    <button class="to-top"><i class="fa fa-chevron-up"></i>TOP</button>

    <div class="restaurant-info">

        <div class="locations">
            <h4>LOCATIONS</h4>
            <div class="location-1">
                <h5>376 Van Brun St<br />Brooklyn, NY &#8212; 11231</h5>
            </div><!--location-1-->
            <div class="location-2">
                <h5>25 Union Square West<br />New York, NY &#8212; 10003</h5>
            </div>
        </div><!--locations end-->

        <div class="hours">
            <h4>HOURS</h4>
            <div class="weekdays">
                <h5>Monday - Thursday<br />
                    5:30pm - 10:00pm
                </h5>
            </div><!--weekdays end-->

            <div class="weekends">
                <h5>Friday & Saturday<br />
                    5:30pm - 11:00pm
                </h5>
            </div><!--weekends end-->

            <div class="private-events">
                <h5>Available for private<br />
                    events on Sunday
                </h5>
            </div>

        </div><!--hours end-->

    </div><!--restaurant-info end-->

    <div class="copyright">
        <p><small>Copyright 2014 &#169; Handcrafted with love by <span>PixelGrade</span> Team</small></p>
        <p><small>Permissions and Copyright &#8226; Contact The Team</small></p>
    </div>

</footer>