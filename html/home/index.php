<style>

@import url("https://fonts.googleapis.com/css?family=Alex+Brush|Overpass+Mono:600,700");
@import url("https://fonts.googleapis.com/css?family=PT+Sans");
body {
    overflow-x: hidden;
}

/*
  GLOBAL
*/
h2 {
    margin-top: 0;
}

button {
    border: 0;
    background: 0;
    cursor: pointer;
}

a {
    text-decoration: none;
    padding-bottom: 5px;
}

ul {
    list-style: none;
    padding: 0;
    margin: 0;
}

/*
  HEADER
*/
header {
    min-height: 100vh;
    background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0.8)), to(transparent)), url("http://www.librojuridicos.com/wp-content/uploads/2017/10/can-web-designers-work-from-home-handsome-hipster-modern-man-designer-working-home-using-of-can-web-designers-work-from-home.jpg") no-repeat center center;
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0.8), transparent), url("http://www.librojuridicos.com/wp-content/uploads/2017/10/can-web-designers-work-from-home-handsome-hipster-modern-man-designer-working-home-using-of-can-web-designers-work-from-home.jpg") no-repeat center center;
    background-size: cover;
    background-attachment: fixed;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    /*
      NAV
    */
    /*
      CENTER HEADER TEXT
    */
}
header nav {
    padding: 15px 35px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    position: relative;
}
header nav h2 {
    margin-bottom: 0;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    letter-spacing: 3px;
    font-family: "Overpass Mono", monospace !important;
}
header nav h2 #logo {
    color: lightgray;
    text-transform: uppercase;
    border-bottom: solid 2px lightgray;
}
header nav .nav-button {
    color: lightgray;
    font-size: 23px;
}
header nav div {
    background: rgba(0, 0, 0, 0.9);
    position: fixed;
    right: 0;
    left: 0;
    top: 0;
    bottom: 0;
    z-index: 1;
    text-align: center;
    -webkit-transform: scale(0);
    transform: scale(0);
    opacity: 0;
    -webkit-transition: opacity .2s;
    transition: opacity .2s;
}
header nav div ul {
    position: relative;
    height: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
header nav div ul .exit-menu {
    position: absolute;
    top: 15px;
    right: 15px;
    color: lightgray;
    font-size: 20px;
    padding: 10px;
}
header nav div ul li {
    font-family: "PT Sans", sans-serif !important;
    text-transform: uppercase;
}
header nav div ul li a {
    display: block;
    color: lightgray;
    padding: 15px 5px;
}
header .center {
    line-height: 0;
    margin: auto;
    text-align: center;
    color: white;
}
header .center h1 {
    font-family: "Alex Brush", cursive !important;
    color: #be8040;
    font-size: 80px;
    margin-bottom: 34px;
}
header .center h2 {
    text-transform: uppercase;
    letter-spacing: 5px;
    font-size: 28px;
}
header .center #asterisk {
    color: #be8040;
    line-height: 0.5;
    font-size: 30px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
header .center #asterisk:before, header .center #asterisk:after {
    content: "";
    display: inline-block;
    position: relative;
    vertical-align: middle;
    height: 2px;
    width: 40%;
    background: white;
}
header .center #asterisk:before {
    margin-right: 10px;
}
header .center #asterisk:after {
    margin-left: 10px;
}
header .center p {
    text-transform: uppercase;
    margin-top: 5px;
    font-size: 14px;
    font-weight: bold;
    letter-spacing: 3px;
}

nav div.show {
    -webkit-transform: scale(1);
    transform: scale(1);
    opacity: 1;
}

/*
  SECTIONS
*/
/*
  CLASSES
*/
.custom-font {
    font-family: "Alex Brush", cursive;
    color: #be8040;
    font-size: 75px;
    line-height: 0;
}

.add-padding {
    padding: 25px;
}

.center-text {
    text-align: center;
    margin-bottom: 40px;
    margin-top: 30px;
    color: #4d4d4d;
}
.center-text h2 {
    margin-top: 25px;
    line-height: 0;
}
.center-text p {
    margin-top: 0;
    font-family: "PT Sans", sans-serif;
    line-height: 1.5;
}
.center-text a {
    font-family: "PT Sans", sans-serif;
    color: #be8040;
    text-transform: uppercase;
    font-size: 12px;
    font-weight: 600;
    letter-spacing: 1px;
    border-bottom: solid 2px #be8040;
}

.add-flex {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}
.add-flex .stuffed-cherries {
    background: url("https://www.sunnyhoi.com/app/uploads/2017/04/freelance-programmers.jpg") no-repeat center center;
    background-size: cover;
    height: 315px;
    width: 320px;
}

.center-h1 {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.custom-h1 {
    text-align: center;
    color: white;
    letter-spacing: 5px;
    margin-bottom: 0;
}
.custom-h1 span {
    letter-spacing: 0;
}

/*
  TASTEFUL RECIPES SECTION
*/
.bread-background {
    background: black url("https://cdn.lynda.com/static/landing/images/hero/Web_designer_1200x630-1503435812380.jpg") no-repeat center center;
    background-size: cover;
    background-attachment: fixed;
    height: 500px;
}

/*
  MENU SECTION
*/
.menu {
    padding: 30px;
}
.menu-images {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 10px;
}
.menu-images img {
    width: 100%;
}

/*
  THE PERFECT BLEND SECTIONkfdkjsjkalñkdjf
*/
.cake-background {
    background: black url("https://cdn.clarivate.com/wp-content/uploads/2017/04/Web-of-Science-Author-Connect_2.0_thumbnail-1.jpg") no-repeat center center;
    background-size: cover;
    background-attachment: fixed;
    height: 500px;
}

/*
  CULINARY DELIGHT SECTION
*/
.reservation-section {
    margin-bottom: 40px;
}
.reservation-section div:first-child {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
.reservation-section div:last-child {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
.reservation-section div:last-child img {
    margin: auto;
    width: 85%;
}

/*
  FOOTER
*/
footer {
    margin: 0;
    position: relative;
    background: #141414;
    color: lightgray;
}
footer button {
    width: 50px;
    height: 50px;
    position: absolute;
    top: -20px;
    left: 50%;
    -webkit-transform: translate(-50%, -10%);
    transform: translate(-50%, -10%);
    background: white;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 12px;
    font-weight: bold;
    letter-spacing: 1.2px;
    line-height: 1.2;
    border-radius: 50%;
    margin-top: 0;
}
footer h4 {
    margin-bottom: 0;
}
footer h5 {
    line-height: 1.5;
}
footer .restaurant-info {
    padding: 35px 40px !important;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}
footer .copyright {
    background: #232323;
    color: #969696;
    font-size: 14px;
    letter-spacing: 1px;
    padding: 10px 30px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    text-align: center;
}
footer .copyright span {
    border-bottom: solid 1px;
}

/*
  MEDIA QUERIES
*/
@media screen and (max-width: 425px) {
    header, .bread-background, .cake-background {
        background-attachment: scroll;
    }
}
@media (min-width: 768px) {
    footer .restaurant-info {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-pack: distribute;
        justify-content: space-around;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
}
@media (min-width: 769px) {
    /*
      NAV
    */
    nav {
        position: initial !important;
        -webkit-box-align: baseline;
        -ms-flex-align: baseline;
        align-items: baseline;
        padding: 20px 70px !important;
    }
    nav .nav-button {
        display: none;
    }
    nav div {
        position: initial !important;
        background: none !important;
        opacity: initial !important;
        -webkit-transform: initial !important;
        transform: initial !important;
    }
    nav div ul {
        position: initial !important;
        -webkit-box-orient: horizontal !important;
        -webkit-box-direction: normal !important;
        -ms-flex-direction: row !important;
        flex-direction: row !important;
    }
    nav div ul .exit-menu {
        display: none;
    }
    nav div ul li a {
        font-size: 12.8px;
        margin: 0 10px;
        letter-spacing: 1px;
        padding: 5px 0 !important;
    }
    nav div ul .active {
        border-bottom: solid 1px lightgray;
    }

    .add-padding {
        padding: 50px 0;
    }

    .add-flex {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .add-flex div:first-child {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        padding: 0 50px;
    }
    .add-flex div:last-child {
        min-width: 400px;
        margin: 50px;
    }

    .menu-images {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }
    .menu div:last-child {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    /*
      FOOTER
    */
    footer .restaurant-info {
        padding: 20px 80px;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    footer .restaurant-info .locations {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }
    footer .restaurant-info .locations h4 {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 100%;
        flex: 1 0 100%;
    }
    footer .restaurant-info .hours {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }
    footer .restaurant-info .hours h4 {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 100%;
        flex: 1 0 100%;
    }
    footer .copyright {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
}
</style>

<?php include(HTML_DIR."shared/header.php") ?>

<section class="add-padding add-flex">
    <div class="center-text">
        <h1 class="alex-brush"><span class="custom-font">Discover</span><br />OUR STORY</h1>
        <h2>*</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit aspernatur beatae laboriosam dicta tempora ab, dolorem mollitia perspiciatis, deleniti quidem dolor repellat animi. Quidem eligendi iste distinctio fugit maxime modi. Lorem ipsum dolor sit amet.</p>
        <a href="#">about us</a>
    </div>
    <div  class="stuffed-cherries" data-aos="fade-left" data-aos-delay="300">
    </div>
</section>

<section class="bread-background center-h1">
    <h1 class="custom-h1 alex-brush"><span class="custom-font">Tasteful</span><br />RECIPES</h1>
</section>

<section class="menu add-flex add-padding">
    <div class="menu-images">
        <img style="width: 250px;" src="https://cdn.shopify.com/s/files/1/0211/8212/products/python_sticker_large.png?v=1439351157"  data-aos="fade-down" data-aos-delay="300"/>
        <img style="width: 250px;"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png" data-aos="fade-left" data-aos-delay="300"/>
        <img style="width: 200px;"  src="https://camo.githubusercontent.com/b6ba9075a54c192efc59bba53c92e7c23ec8cfe8/68747470733a2f2f63646e2e7261776769742e636f6d2f67696c626172626172612f6c6f676f732f653762316463323636366333646162653663313237366162643061373637623665626436616634332f6c6f676f732f6e6f64656a732d69636f6e2e737667" data-aos="fade-right" data-aos-delay="300"/>
        <img style="width: 250px;"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Julia_prog_language.svg/1200px-Julia_prog_language.svg.png" data-aos="fade-up" data-aos-delay="300"/>
    </div>
    <div class="center-text">
        <h1><span class="custom-font alex-brush">Discover</span><br />MENU</h1>
        <h2>*</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio possimus tempore voluptatem, quo repellendus quas culpa quasi, hic optio sapiente molestias necessitatibus, aliquam excepturi consequatur a voluptates quam beatae!</p>
        <a href="#">view the full menu</a>
    </div>
</section>

<section class="cake-background center-h1">
    <h1 class="custom-h1"><span class="custom-font alex-brush">The perfect</span><br />BLEND</h1>
</section>

<section class="add-flex reservation-section">
    <div class="center-text add-padding">
        <h1><span class="custom-font alex-brush">Culinary</span><br />DELIGHT</h1>
        <h2>*</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim animi odit in dignissimos neque ratione, laboriosam rerum! Deleniti accusamus non, aliquam tempora, mollitia laborum ad, fugiat at explicabo esse aut. Lorem ipsum dolor sit amet, adipisicing elit.</p>
        <a href="#">make a reservation</a>
    </div>
    <div>
        <img src="http://fossbytes.com/wp-content/uploads/2016/02/learn-to-code-what-is-programming.jpg" data-aos="fade-up" data-aos-delay="300"/>
        <img src="https://www.elegantthemes.com/blog/wp-content/uploads/2017/07/programming-languages-to-learn-for-wordpress-featured-image.png" data-aos="fade-down" data-aos-delay="300"/>
    </div>
</section>

<?php include(HTML_DIR."shared/footer.php") ?>

<script>

    // by: George Olaru https://dribbble.com/shots/1560982-Rosa-Restaurant-Website/attachments/239212

    $('button').on('click', function() {
        if($(this).hasClass('nav-button')) {
            $('nav div').addClass('show');
        } else if($(this).hasClass('exit-menu')) {
            $('nav div').removeClass('show');
        }
        else if($(this).hasClass('to-top')) {
            $('html,body').animate({scrollTop:0}, 'slow');
        }
    });

    AOS.init({
        duration: 1800,
        easing: 'ease'
    });

</script>
